# installer script

`installer script` es una *plantilla* pensada para ayudarte a crear el script de instalación de las aplicaciones que desarrolles para un sistema GNU/linux.

> *¿Y qué programas?*  
> Cualquiera. Desde aplicaciones gráficas a un bash script.

Es un script genérico, una suerte de *plantilla* de script. `installer script` simplemente copia los archivos de tu aplicación a la ubicación correcta del sistema GNU/Linux donde la quieras instalar. Está preconfigurado para sistemas basados en Debian GNU/Linux, pero puede adaptarse a cualquier otra jerarquía de archivos.

`installer script` también sirve para desinstalar el programa.

### Descargarse "installer script"

Clona el repositorio...

`sudo apt-get install git` (sólo si no tienes git instalado)

`git clone https://gitlab.com/juansv/installer_script.git`

En el repositorio de gitlab están los siguientes archivos y directorios:

- *installer_script.sh* Este es el script propiamente dicho
- *example-1.0* (y varios directorios relacionados) Este es un pequeño programa en python que sirve para entender cómo funciona el script.

### Uso de "installer script"

`installer script` viene acompañado de un programa de ejemplo, llamado `example`. Este programa no hace nada, sólo saca un mensaje por la terminal. En esta explicación de uso se harán contínuas referencias a `example-1.0` para ilustrar el funcionamiento. El script que te descargarás de este repositorio gitlab está configurado para instalar `example-1.0`.

Para usar el instalador sólo hay que hacer dos cosas:

1. Indicar los directorios de destino de tu sistema GNU/Linux (si es compatible con la jerarquía Debian, no hay que cambiar nada). Esto se hace en las líneas de la 17 a la 23.
2. Rellenar las variables que representan a los archivos y directorios del programa a instalar. Esto se hace en las líneas de la 30 a la 41.

Veamos estas dos partes en detalle:

#### Directorios de destino

En las líneas de la 17 a la 23 de `installer_script.sh` figura el siguiente código:

~~~
launchers_dest='/usr/bin'
sources_dest='/usr/share'
man_dest='/usr/share/man'
doc_dest='/usr/share/doc'
icon_dest='/usr/share/pixmaps'
locale_dest='/usr/share/locale'
desktop_dest='/usr/share/applications'
~~~

En estas líneas hay 7 variables que representan los directorios donde se guardan los archivos de cualquier programa que se instale en el sistema GNU/Linux que se trate. Estos directorios deben existir. Y deben ser los directorios superiores, es decir, no deben ser directorios particulares de nuestro programa.

No importa si nuestro programa, por ejemplo, no tiene iconos. Podemos rellenar el directorio de destino de los iconos igualmente. El script ignorará este directorio de destino si el programa no tuviese iconos. Esto ocurre en el programa `example`.

#### Directorios y archivos del programa a instalar

En las líneas de la 30 a la 41 hay 12 variables que representan los directorios y archivos del programa a instalar. Al lado de cada variable hay un comentario que explica el destino final del archivo o directorio que hay detrás de la variable.

La variable con el nombre del programa es obligatoria, aunque no representa ningún archivo en particular. No es necesario que el programa a instalar tenga todos estos archivos. Si alguno no aplica, basta con dejar su variable en blanco; `example` no tiene iconos, ni archivo `.desktop`, ni entradas del `man`, por eso sus variables están igualadas a `=''`.

~~~
app_name='example-1.0' # For information purpose but compulsory
dir_src='example-1.0' # => sources_dest
dir_app='' # => sources_dest
dir_launch='bin' # => launchers_dest
dir_doc='doc' # => doc_dest
dir_man='' # => man_dest
dir_locale='locale' # => locale_dest
dir_icon='' # => icon_dest
dir_desktop='' # => desktop_dest
file_readme='README.md' # => doc_dest
file_license='' # => doc_dest
file_leeme='' # => doc_dest
~~~

Los directorios del programa a instalar deben tener la misma estructura que tendrán una vez instalados. `example` tiene un archivo para la traducción al castellano. Este archivo está en la ruta `./locale/es/LC_MESSAGES/example-1.0.mo`. El instalador copiará toda esta ruta en el directorio representado por la variable `locale_dest`, es decir, en `/usr/share/locale`. El resultado final será que el archivo de traducción `example-1.0.mo` se instalará en la ruta `/usr/share/locale/es/LC_MESSAGES/example-1.0.mo`

Los archivos `LICENSE`, `README.md` y `LEEME.md` se copiarán como parte de la documentación del programa en `/usr/share/doc/example-1.0`.

#### Desinstalar el programa

Cuando ejecutamos `installer_script.sh` se instalará `example`. Pero también se creará un archivo llamado `installed.list`. Este archivo es un listado de todos los archivos instalados. Si ejecutamos nuevamente `installer_script.sh`, podremos elegir la opción *desinstalar* y el script buscará los archivos relacionados en `installed.list` y los eliminará del sistema.

Obviamente, sin el archivo `installed.list` no se puede desinstalar el programa. Por ese motivo, durante la instalación se recuerda al usuario que conserve `installed.list` junto con el script de instalación.

### Tecnología

`installer script` es un script para la `bash shell`.

El nombre `installer_script.sh` puede cambiarse sin problemas, no es relevante para su funcionamiento. De esta forma puedes usar `installer script` como base para todos tus programas.

Todos los mensajes que lanza `installer script` por pantalla lo hace con un `echo`. Por defecto, los mensajes de `installer script` están en inglés. Pero al lado de cada `echo` hay otra línea comentada con el mismo mensaje en castellano. Solo hay que comentar o descomentar la línea adecuada para cambiar el idioma.

`installer script` se compone de las siguientes funciones:

| Función | Qué hace |
| --- | --- |
| `whattodo` | Muestra el menú principal del instalador |
| `files_place` | Comprueba que los directorios de destino existen |
| `files_ok` | Comprueba que los archivos y directorios del programa existen |
| `copyfiles` | Copia los arhivos y directorios de programa en los directorios de destino |
| `listinstalled` | Crea la lista de archivos instalados `installed.list` |
| `files_in` | Comprueba que los archivos de `installed.list` están instalados en el sistema |
| `deletefiles` | Borra del sistema los archivos de `installed.list` |

El código principal del script comienza en la línea 471. El código simplemente comprueba que el script se lanzó como `sudo` y luego ejecuta las funciones de arriba, empezando por la función del menú `whattodo`. Según la opción del menú que se elija, se ejecutan unas u otras funciones.

En la línea 465 pueden insertarse más funciones. Por ejemplo, funciones que creen un archivo de configuración, que modifiquen permisos sobre archivos del programa, etc. Estas funciones deben incluirse en el código principal del script (desde línea 471) en el lugar que le corresponda.

### Versiones

#### 1.0

Versión inicial

### Autor y licencia (Author and license)

Copyright 2023 [Juan Antonio Silva Villar](mailto:juan.silva@disroot.org).

Licencia GPLv3+: GNU GPL version 3 o posterior <https://gnu.org/licences/gpl.html>.

