#!/usr/bin/env python3
# -*- coding: utf-8 -*-

##############################################################################
# Este programa es un script Python sencillo pensado para ilustrar cómo
# funciona el script "generic installer script"
#
# This is a simple Python script made to show the way the "generic installer
# script" works
#
# Copyright (C) 2022 Juan Antonio Silva Villar <juan.silva@disroot.org>
#
# Este programa es software libre; puede redistribuirlo y/o modificarlo bajo
# los términos de la Licencia Pública General GNU, tal y como está publicada
# por la Free Software Foundation, ya sea la versión 3 de la licencia o (a
# su elección) cualquier versión posterior.
#
# Este progama se distribuye con la intención de ser útil, pero SIN NINGUNA
# GARANTÍA; ni siquiera la garantía implícita de COMERCIALIZACIÓN o
# UTILIDAD PARA UN FIN PARTICULAR. Consulte la Licencia Pública General GNU
# para más detalles.
#
# Debería haber recibido una copia de la Licencia Pública General GNU junto
# con este programa. Si no es así, consulte <http://www.gnu.org/licenses/>.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
##############################################################################

# Módulos estándar
import gettext

# Configuración de las traducciones
gettext.textdomain("example-1.0")
gettext.bindtextdomain("example-1.0", "/usr/share/locale")
# Alias para las traducciones.
_ = gettext.gettext

def main():
    print(_("I just did nothing"))
    print(_("I am only an example of the generic installer script"))

if __name__ == "__main__":
    main()