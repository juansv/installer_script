# installer script

`installer script` is an script *template* to help you to make the installer for the applications developed by you. It is only for GNU/Linux systems.

> *and, what kind of apps is valid for?*  
> Whatever. From graphics apps to simple bash scripts.

`installer script` simply copies your app files into destination directories of the GNU/Linux system. It is configured for Debian GNU/Linux, but it could be adapted to whatever files hierarchy.

`installer script` also uninstall your app.

### Download "installer script"

Clone the repository...

`sudo apt-get install git` (just only if you don't have git installed)

`git clone https://gitlab.com/juansv/installer_script.git`

The gitlab repository includes the following:

- *installer_script.sh* This is the script properly.
- *example-1.0* (and several related files) This is a small and simple python app to show how the installer works.

### Use of "installer script"

`installer script` comes with an app called `example`. This app does nothing but show a message in the terminal. But continuos references to `example-1.0` are made here to illustrate how the script works. The script you clone from gitlab repository is configured to install `example-1.0`.

Using the installer means doing:

1. Point out destination directories. It should be done in lines from 17 to 23. In case of Debian hierarchy files compatible systems there is nothing to do.
2. Fill in the variables which portray files and directories to be installed. It should be done in lines from 30 to 41.

Let see these two things in detail

#### Destination directories

`installer_script.sh` lines from 17 to 23 has the following code:

~~~
launchers_dest='/usr/bin'
sources_dest='/usr/share'
man_dest='/usr/share/man'
doc_dest='/usr/share/doc'
icon_dest='/usr/share/pixmaps'
locale_dest='/usr/share/locale'
desktop_dest='/usr/share/applications'
~~~

They are seven variables which represent GNU/Linux system directories where install my app files. It should be the parent directories and they should be exist.

No matter if our app do not have something for those directories. For instance, `example` has no icons but icons destination directory `/usr/share/pixmaps` is included.

#### Application files and directories

Lines from 30 to 41 show 12 variables which portray directories and files of our app to be installed. Besides each variable there is a comment which point to destination directory.

App name variable is compulsory, although it does not represent any file. It is not necessary that our app has all those files. If some variables are not needed, leave the variable like this `=''`. `example` has no icons, has no `.desktop` file and has no `man` entries, that's why these variables equals `=''`.

~~~
app_name='example-1.0' # For information purpose but compulsory
dir_src='example-1.0' # => sources_dest
dir_app='' # => sources_dest
dir_launch='bin' # => launchers_dest
dir_doc='doc' # => doc_dest
dir_man='' # => man_dest
dir_locale='locale' # => locale_dest
dir_icon='' # => icon_dest
dir_desktop='' # => desktop_dest
file_readme='README.md' # => doc_dest
file_license='' # => doc_dest
file_leeme='' # => doc_dest
~~~

App directories should have the same relative path as they will have ones they would be installed. `example` has a file for spanish translation. This file is at `./locale/es/LC_MESSAGES/example-1.0.mo`. The installer will copy this path to the directory portray by `locale_dest`, that is `/usr/share/locale`. That way, spanish file `example-1.0.mo` will be placed at `/usr/share/locale/es/LC_MESSAGES/example-1.0.mo`.

`LICENSE`, `README.md` and `LEEME.md` will be copied as part of app documentation at `/usr/share/doc/example-1.0`.

#### Uninstall

When we install `example` by using `installer_script.sh`, a file called `installed.list`, with the list of installed files, will be done. Then, if we run `installer_script.sh` again, we could chose *uninstall* in the script menu. The script looks for the files listed into `installed.list` and they will be removed from the system.

Obviously, without `installed.list` is not possible uninstall the app. That's the reason why a warning message is shown when the app is installing.

### Technical

`installer script` is an script for the `bash shell`.

You can change the name `installer_script.sh`, it does not affect the behavior. That way you can name the script as your app installer script.

All messages `installer script` shows on screen were send by an `echo`. They are in English by default.

`installer script` have the following functions:

| Function | What it does |
| --- | --- |
| `whattodo` | Shows installer main menu |
| `files_place` | Checks if destination directories exist |
| `files_ok` | Checks if app files and directories exist |
| `copyfiles` | Copies app files and directories into destination directories |
| `listinstalled` | Creates `installed.list` |
| `files_in` | Checks if the files of `installed.list` are installed |
| `deletefiles` | Deletes all files portrays in `installed.list` |

The script main code starts at line 471. The code simply checks if script was run as `sudo` and then executes the functions above, starting from `whattodo`. The rest of functions will be executed depending on the chosen menu option.

It is possible to add more functions from line 465 on. For instance, if you need to create a config file, you can insert it there. These new functions should be called from the main code (from line 471 on) so they should be place in the right menu option.

### Versions

#### 1.0

Initial version.

### Author and license

Copyright 2023 [Juan Antonio Silva Villar](mailto:juan.silva@disroot.org).

License GPLv3+: GNU GPL version 3 or later <https://gnu.org/licences/gpl.html>.

