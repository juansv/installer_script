#!/bin/bash
#
# Script de instalación genérico
# Generic installer script
# Copyleft: Juan Antonio Silva Villar <juan.silva@disroot.org>
# License: GPLv3
#
# Este instalador es muy simple, sólo copia los archivos a los directorios del sistema para integrarlo. Las rutas de los directorios corresponden con un SO Debian 11 GNU/linux. Si tu SO corresponde con otra distribución, tal vez debas ajustar las rutas. Debes ejecutar el script como sudo para que se puedan copiar los archivos a directorios del sistema.
#
# This install script is so simple. It only copies all files to their system directories to fit in. Directories paths match up with Debian 11 GNU/linux OS. If you use another OS, maybe you have to change all/some paths. You have to run this script as sudo in order to be able to copy into system directories.
#
#######################################################################################
# DIRECTORIOS DE DESTINO
# DESTINATION DIRECTORIES
# Si no utilizas Debian GNU/linux o usas otros directorios como destino para tu software, modifica las siguientes variables sin más que indicar la nueva ruta
# If you don't use Debian GNU/linux or you prefer another directories as destination for you software, please, modify the following variables by changing the path it points
launchers_dest='/usr/bin'
sources_dest='/usr/share'
man_dest='/usr/share/man'
doc_dest='/usr/share/doc'
icon_dest='/usr/share/pixmaps'
locale_dest='/usr/share/locale'
desktop_dest='/usr/share/applications'
#
#######################################################################################
# DIRECTORIOS Y ARCHIVOS DEL PROGRAMA
# PROGRAM FILES AND DIRECTORIES
# Si el proyecto no requiere de alguna de estas variables, dejar ''. Al lado se indica, comentado, a qué punto será copiado el directorio/archivo.
# If any variable is not needed, type ''. Destination is pointed out beside each variable as comment.
app_name='example-1.0' # For information purpose but compulsory
dir_src='example-1.0' # => sources_dest
dir_app='' # => sources_dest
dir_launch='bin' # => launchers_dest
dir_doc='doc' # => doc_dest
dir_man='' # => man_dest
dir_locale='locale' # => locale_dest
dir_icon='' # => icon_dest
dir_desktop='' # => desktop_dest
file_readme='README.md' # => doc_dest
file_license='' # => doc_dest
file_leeme='' # => doc_dest
#
#######################################################################################
# FUNCIONES COMUNES
# COMMON FUNCTIONS
# A partir de este punto no es necesario tocar nada en el script. Sólo en el caso que su programa tenga más archivos o directorios de los previstos en el apartado DIRECTORIOS Y ARCHIVOS DEL PROGRAMA podría necesitar modificar algo en el script.
# From this point on, you do not normally need to change anything. Only if you have more files and directories than those of PROGRAM FILES AND DIRECTORIES chapter you will need to modify something.
#
# ==============================
# Función para el menú principal
# Main menu function
# ==============================
whattodo(){
    local option='e'
#    echo "Bienvenido al instalador de $app_name"
#    echo "Por favor, elige una opción"
    echo "Welcome to $app_name installer"
    echo "Please, choose an option"
    read -p "Install/Uninstall/Exit [i/u/E]: " option
    case $option in
        i)
            mustdo='Install'
            ;;
        u)
            mustdo='Uninstall'
            ;;
        e)
            mustdo='Exit'
            ;;
        "")
            mustdo='Exit'
            ;;
    esac
}
#
# ================================================
# Comprueba que los directorios de destino existen
# Check if destination directories exist
# ================================================
files_place(){
#    echo "Verificando directorios de destino..."
    echo "Checking destination directories..."
    ls $launchers_dest > /dev/null 2>&1
    out_launchers="$?"
    ls $sources_dest > /dev/null 2>&1
    out_sources="$?"
    ls $man_dest > /dev/null 2>&1
    out_man="$?"
    ls $doc_dest > /dev/null 2>&1
    out_doc="$?"
    ls $icon_dest > /dev/null 2>&1
    out_icon="$?"
    ls $locale_dest > /dev/null 2>&1
    out_locale="$?"
    ls $desktop_dest > /dev/null 2>&1
    out_desktop="$?"
    dest_result=$(($out_launchers+$out_sources+$out_man+$out_doc+$out_icon+$out_locale+$out_desktop))
    if [ "$dest_result" != 0 ]; then
#        echo "Este script de instalación está configurado para Debian GNU/linux"
#        echo "Por favor, ajusta los directorios de destino en el script de instalación"
        echo "This install script is configured for Debian GNU/linux"
        echo "Please, check and change destination directories in this install script"
        exit 1
    else
#        echo "Directorios de destino OK"
        echo "Directories OK"
    fi
}
#
# ======================================================================
# Comprueba que los directorios y archivos de programa están descargados
# Check if all program directories and files have been downloaded
# ======================================================================
files_ok(){
#    echo "Verificando archivos..."
    echo "File checking..."
    #
    # Directorio donde se guardan las fuentes
    if [ "$dir_src" != '' ]; then
        ls ./$dir_src/* > /dev/null 2>&1
        out_src="$?"
        if [ "$out_src" = 0 ]; then echo "$dir_src -> OK"; else echo "$dir_src -> *KO*"; fi
    else
        out_src='0'
    fi
    #
    # Directorio donde se guardan los binarios del programa (si los hubiese)
    if [ "$dir_app" != '' ]; then
        ls ./$dir_app/* > /dev/null 2>&1
        out_app="$?"
        if [ "$out_app" = 0 ]; then echo "$dir_app -> OK"; else echo "$dir_app -> *KO*"; fi
    else
        out_app='0'
    fi
    #
    # Directorio donde se guardan el/los lanzadores del programa
    if [ "$dir_launch" != '' ]; then
        ls ./$dir_launch/* > /dev/null 2>&1
        out_launch="$?"
        if [ "$out_launch" = 0 ]; then echo "$dir_launch -> OK"; else echo "$dir_launch -> *KO*"; fi
    else
        out_launch='0'
    fi
    #
    # Directorio donde se guarda la documentación del programa
    if [ "$dir_doc" != '' ]; then
        ls ./$dir_doc/* > /dev/null 2>&1
        out_doc="$?"
        if [ "$out_doc" = 0 ]; then echo "$dir_doc -> OK"; else echo "$dir_doc -> *KO*"; fi
    else
        out_doc='0'
    fi
    #
    # Directorio donde se guardan los man, tanto en inglés como en español, del programa
    if [ "$dir_man" != '' ]; then
        ls ./$dir_man/* > /dev/null 2>&1
        out_man="$?"
        if [ "$out_man" = 0 ]; then echo "$dir_man -> OK"; else echo "$dir_man -> *KO*"; fi
    else
        out_man='0'
    fi
    #
    # Directorio donde se guardan los .mo con las traducciones a los distintos idiomas
    if [ "$dir_locale" != '' ]; then
        ls ./$dir_locale/* > /dev/null 2>&1
        out_locale="$?"
        if [ "$out_locale" = 0 ]; then echo "$dir_locale -> OK"; else echo "$dir_locale -> *KO*"; fi
    else
        out_locale='0'
    fi
    #
    # Directorio donde se guardan los iconos del programa
    if [ "$dir_icon" != '' ]; then
        ls ./$dir_icon/* > /dev/null 2>&1
        out_icon="$?"
        if [ "$out_icon" = 0 ]; then echo "$dir_icon -> OK"; else echo "$dir_icon -> *KO*"; fi
    else
        out_icon='0'
    fi
    #
    # Directorio donde se guardan los archivos .desktop para lanzar desde el entorno gráfico
    if [ "$dir_desktop" != '' ]; then
        ls ./$dir_desktop/* > /dev/null 2>&1
        out_desktop="$?"
        if [ "$out_desktop" = 0 ]; then echo "$dir_desktop -> OK"; else echo "$dir_desktop -> *KO*"; fi
    else
        out_desktop='0'
    fi
    #
    # Archivo README.md que se cuelga en el repositorio git remoto
    if [ "$file_readme" != '' ]; then
        ls ./$file_readme > /dev/null 2>&1
        out_readme="$?"
        if [ "$out_readme" = 0 ]; then echo "$file_readme -> OK"; else echo "$file_readme -> *KO*"; fi
    else
        out_readme='0'
    fi
    #
    # Archivo con la licencia
    if [ "$file_license" != '' ]; then
        ls ./$file_license > /dev/null 2>&1
        out_license="$?"
        if [ "$out_license" = 0 ]; then echo "$file_license -> OK"; else echo "$file_license -> *KO*"; fi
    else
        out_license='0'
    fi
    #
    # Archivo LEEME.md equivalente al README.md para el repositorio git remoto
    if [ "$file_leeme" != '' ]; then
        ls ./$file_leeme > /dev/null 2>&1
        out_leeme="$?"
        if [ "$out_leeme" = 0 ]; then echo "$file_leeme -> OK"; else echo "$file_leeme -> *KO*"; fi
    else
        out_leeme='0'
    fi
    #
    # Suma de todas las salidas de los 'ls' anteriores
    result=$(($out_src+$out_app+$out_launch+$out_doc+$out_man+$out_locale+$out_icon+$out_desktop+$out_readme+$out_license+$out_leeme))
    if [ "$result" != 0 ]; then
#        echo "ERROR: falta algún/os archivo/s, comprueba los mensajes KO"
        echo "ERROR: there is/are some file/s missing, check KO messages"
        exit 2
    else
#        echo "Todos los archivos están listos"
        echo "All files are ready"
    fi
}
#
# ==============================================================
# Copia los directorios y los archivos del programa a su destino
# Copy directories and files to their destination point
# ==============================================================
copyfiles(){ 
#    echo "Copiando archivos..."
    echo "Copying files..."
    # Crear el directorio de las fuentes y copiarlas
    # Make the sources directory and copy sources files
    if [ "$dir_src" != '' ]; then
        cp -R ./$dir_src $sources_dest
        # Si lo que cuelga de $dir_src son sólo archivos:
        ls ./$dir_src/ | xargs -I '{}' chmod +x $sources_dest/$dir_src/'{}'
        # Si lo que cuelga de $dir_src son archivos y directorios, descomenta:
#        chmod -R a+x $sources_dest/$dir_src/
#        echo "copiadas las fuentes"
        echo "sources copied"
    fi
    # Crear el directorio de binarios y copiarlos
    # Make the binary directory and copy binary files
    if [ "$dir_app" != '' ]; then
        cp -R ./$dir_app $sources_dest
        # Si lo que cuelga de $dir_app son sólo archivos:
        ls ./$dir_app/ | xargs -I '{}' chmod +x $sources_dest/$dir_app/'{}'
        # Si lo que cuelga de $dir_app son archivos y directorios, descomenta:
#        chmod -R a+x $sources_dest/$dir_app/
#        echo "copiados los binarios"
        echo "binaries copied"
    fi
    # Copiar los scripts que lanzan el programa a /usr/bin
    # Copy launcher scripts to /usr/bin
    if [ "$dir_launch" != '' ]; then
        cp ./$dir_launch/* $launchers_dest
        ls ./$dir_launch/ | xargs -I '{}' chmod +x $launchers_dest/'{}'
#        echo "copiados los lanzadores"
        echo "launchers copied"
    fi
    # Copiar los archivos de man al directorio de los man1
    # Copy man files to man1 directory
    if [ "$dir_man" != '' ]; then
        cp -R ./$dir_man/* $man_dest
#        echo "copiado el man"
        echo "man copied"
    fi
    # Copiar los archivos de las traducciones al directorio de locale
    # Copy locale files to locale directory
    if [ "$dir_locale" != '' ]; then
        cp -R ./$dir_locale/* $locale_dest
#        echo "copiadas las traducciones"
        echo "locale files copied"
    fi
    # Copiar los iconos
    # Copy icons
    if [ "$dir_icon" != '' ]; then
        cp ./$dir_icon/* $icon_dest
#        echo "copiados los iconos"
        echo "icons copied"
    fi
    # Copiar el .desktop
    # Copy .desktop file
    if [ "$dir_desktop" != '' ]; then
        cp ./$dir_desktop/* $desktop_dest
#        echo "copiado el .desktop"
        echo ".desktop file copied"
    fi
    # Crear el directorio de la documentación y copiar la documentación
    # Make the documentation directory and copy documentation files
    if [ "$dir_doc" != '' ]; then
        cp -R ./$dir_doc/* $doc_dest
        local count_dir=$(ls ./$dir_doc | wc -l)
    else
        local count_dir='2'
    fi
    # Si existe dir_doc, comprobamos que sólo cuelgue de él un subdirectorio y asignamos el nombre de dicho subdirectorio a una variable. De lo contrario, llamamos a esta variable con el nombre del directorio de las fuentes.
    if [ "$count_dir" -gt 1 ]; then
        doc_subdir=$dir_src;
    else
        doc_subdir=$(ls ./$dir_doc)
    fi
    # Si existe alguno de los archivos README, LEEME o LICENSE, crea el subdirectorio para la documentación (si no existiese)
    if [ "$file_readme" != '' ] || [ "$file_license" != '' ] || [ "$file_leeme" != '' ]; then
        ls $doc_dest/$doc_subdir > /dev/null 2>&1
        local doc_subdir_exist="$?"
        if [ "$doc_subdir_exist" != 0 ]; then mkdir $doc_dest/$doc_subdir; fi
    fi
    # Procedemos a guardar los archivos README, LEEME o LICENSE en la documentación
    if [ "$file_readme" != '' ]; then
        cp ./$file_readme $doc_dest/$doc_subdir
    fi
    if [ "$file_license" != '' ]; then
        cp ./$file_license $doc_dest/$doc_subdir
    fi
    if [ "$file_leeme" != '' ]; then
        cp ./$file_leeme $doc_dest/$doc_subdir
    fi
    if [ "$dir_doc" != '' ] || [ "$file_readme" != '' ] || [ "$file_license" != '' ] || [ "$file_leeme" != '' ]; then
#        echo "copiada la documentación y licencia"
        echo "documentation and license copied"
    fi
}
#
# =========================================
# Crea una lista de los archivos instalados
# Make a list of installed files
# =========================================
listinstalled(){
#    echo "creando lista de archivos instalados en installed.list..."
    echo "making list of installed files into installed.list..."
    # Creamos la lista de todos los archivos instalados listando
    # los archivos clonados del repositorio git y entregando la lista
    # al comando find para que me devuelva la ruta completa
    # Las fuentes:
    if [ "$dir_src" != '' ]; then
        ls -R ./$dir_src | xargs -I '{}' find $sources_dest -type f -name '{}' >> ./installed.list
    fi
    # Los lanzadores
    if [ "$dir_launch" != '' ]; then
        ls -R ./$dir_launch | xargs -I '{}' find $launchers_dest -type f -name '{}' >> ./installed.list
    fi
    # La documentación
    if [ "$dir_doc" != '' ]; then
        ls -R ./$dir_doc | xargs -I '{}' find $doc_dest -type f -name '{}' >> ./installed.list
    fi
    if [ "$file_readme" != '' ]; then
        find $doc_dest/$doc_subdir -type f -name $file_readme >> ./installed.list
    fi
    if [ "$file_license" != '' ]; then
        find $doc_dest/$doc_subdir -type f -name $file_license >> ./installed.list
    fi
    if [ "$file_leeme" != '' ]; then
        find $doc_dest/$doc_subdir -type f -name $file_leeme >> ./installed.list
    fi
    # Los manuales
    if [ "$dir_man" != '' ]; then
        ls -R ./$dir_man | xargs -I '{}' find $man_dest -type f -name '{}' >> ./installed.list
    fi
    # Las traducciones
    if [ "$dir_locale" != '' ]; then
        ls -R ./$dir_locale | xargs -I '{}' find $locale_dest -type f -name '{}' >> ./installed.list
    fi
    # Los iconos
    if [ "$dir_icon" != '' ]; then
        ls -R ./$dir_icon | xargs -I '{}' find $icon_dest -type f -name '{}' >> ./installed.list
    fi
    # Los archivos desktop
    if [ "$dir_desktop" != '' ]; then
        ls -R ./$dir_desktop | xargs -I '{}' find $desktop_dest -type f -name '{}' >> ./installed.list
    fi
#    echo "creada una lista de archivos instalados en installed.list"
#    echo "por favor, conserve el archivo installed.list junto con este script de instalación"
    echo "list of installed files made and saved in installed.list"
    echo "please, keep installed.list and this installation script together"
}
#
# ========================================================================
# Comprueba si los archivos de la lista están instalados en el ordenador
# Compare files installed in the computer with the list of installed files
# ========================================================================
files_in(){
#    echo "Buscando archivos..."
    echo "Files search..."
    #
    local count='0'
    local out_file='0'
    local file=''
    local list=$(cat ./installed.list)
    for file in $list
    do
        ls $file > /dev/null 2>&1
        out_file="$?"
        if [ "$out_file" = 0 ]; then echo "$file -> OK"; else echo "$file -> *KO*"; fi
        count=$(($count+$out_file))
    done
    if [ "$count" != 0 ]; then
#        echo "ERROR: No se encuentra/n algún/os archivo/s, comprueba los mensajes KO"
#        echo "deberás borrar los archivos manualmente"
        echo "ERROR: Some file/s not found, check KO messages"
        echo "you have to delete it/them manually"
        exit 2
    else
#        echo "Se han encontrado todos los archivos"
        echo "All files were found"
    fi
}
#
# ===========================================================================
# Borra los archivos indicados en la lista, excepto archivos de configuración
# Delete all files related in installed files list except config files
# ===========================================================================
deletefiles(){
    local option='n'
#    echo "Se procederá a borrar todos los archivos que se acaban de listar"
#    echo "si observa algún archivo que no debería ser borrado, por favor, cancele"
#    echo "y edite el archivo installed.list antes de repetir la desinstalación"
    echo "All files have just been listed above will be deleted"
    echo "if you see any file that should not be removed, please, quit"
    echo "then edit installed.list before trying the uninstall process again"
    read -p "Continue? [y/n]: " option
    case $option in
        y)
#            echo "Borrando archivos..."
            echo "deleting..."
            local file=''
            local list=$(cat ./installed.list)
            for file in $list
            do
                rm $file
            done
#            echo "los archivos han sido borrados"
#            echo "los directorios pueden permanecer"
            echo "all files have been deleted"
            echo "directories could remain"
            ;;
        n)
#            echo "Se cancela la desinstalación"
            echo "Uninstalling canceled"
            exit 0
            ;;
        "")
#            echo "Se cancela la desinstalación"
            echo "Uninstalling canceled"
            exit 0
            ;;
    esac
}
#
# ============================
# Fin de las funciones comunes
# Common functions end
# ============================
#
#######################################################################################
# FUNCIONES ESPECÍFICAS DEL PROGRAMA
# NON COMMON FUNCTIONS
# Si se necesita alguna/s función/es específica/s, se indican aquí
# If more functions were needed, they should be written from this point on
#
# ================================
# Fin de las funciones específicas
# Non-common functions end
# ================================
#
#######################################################################################
# PROGRAMA PRINCIPAL
# MAIN CODE
#
# Comprobando la ejecución como sudo
# Checking if run as sudo
if [ "$(id -u)" != "0" ]; then
#    echo "Por favor, ejecuta este script como root."
    echo "Please, must be run as root."
    exit 1
else
    mustdo='Exit'
    whattodo
    case $mustdo in
        Install)
#            echo "Instalando $app_name..."
            echo "Instaling $app_name..."
            files_place
            files_ok
            copyfiles
            listinstalled
#            echo "Instalación de $app_name terminada con éxito."
            echo "$app_name was installed successfully."
            exit 0
            ;;
        Uninstall)
#            echo "Desinstalando $app_name..."
            echo "Uninstaling $app_name..."
            files_in
            deletefiles
#            echo "Desinstalación de $app_name completada con éxito."
#            echo "Los archivos de configuración y los directorios permanecen."
            echo "$app_name was uninstalled successfully."
            echo "Config files and directories remain."
            exit 0
            ;;
        Exit)
#            echo "¡Adios!"
            echo "Good bye!"
            exit 0
            ;;
    esac
fi

